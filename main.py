import mysql.connector
import re
import json
import sqlfluff
import toml
import os


def scan_query(
    connection: mysql.connector.MySQLConnection,
    query_json,
):
    messages = []

    def identify_wildcard_select(
        _connection: mysql.connector.MySQLConnection,
        _query: str,
    ):
        wildcard_select_pattern = re.compile(r'\W?SELECT\W.*?(?<!COUNT\()\*.*?\WFROM', re.IGNORECASE | re.DOTALL)

        if re.match(wildcard_select_pattern, _query):
            messages.append('- Wildcard select detected')

    def identify_wildcard_like(
        _connection: mysql.connector.MySQLConnection,
        _query: str,
    ):
        wildcard_like_pattern = re.compile(r'\WLIKE\W+%\w+', re.IGNORECASE | re.DOTALL)

        if re.match(wildcard_like_pattern, _query):
            messages.append('- Wildcard like detected')

    def identify_full_table_scan(
        _connection: mysql.connector.MySQLConnection,
        _query: str,
    ):
        if 'SET' == _query.lstrip()[:4].upper():
            return

        if 'CALL' == _query.lstrip()[:4].upper():
            return

        with _connection.cursor() as cursor:
            try:
                cursor.execute(f'EXPLAIN {_query}')
            except mysql.connector.Error as err:
                messages.append(f'- Error: {err.msg}')

            explain_result = cursor.fetchall()

            if any('ALL' in row for row in explain_result):
                messages.append('- Full table scan detected')

    def identify_call_store_procedure(
        _connection: mysql.connector.MySQLConnection,
        _query: str,
    ):
        if 'CALL' == _query.lstrip()[:4].upper():
            messages.append('- Stored procedure detected')

    def identify_slow_query(
        _connection: mysql.connector.MySQLConnection,
        _query_time: float,
    ):
        # Threshold on 10ms
        if _query_time > .01:
            messages.append('- Slow query detected')

    identify_wildcard_select(connection, query_json['query'])
    identify_wildcard_like(connection, query_json['query'])
    identify_full_table_scan(connection, query_json['query'])
    identify_call_store_procedure(connection, query_json['query'])
    identify_slow_query(connection, query_json['time'])

    return messages


def main():
    config_path = './config.toml'

    with open(config_path, 'r') as file:
        config = toml.load(file)

    def output(value):
        nonlocal config

        if 'result_file_path' in config['log'] and config['log']['result_file_path'] != 'stdout':
            result_file_path = config['log']['result_file_path']

            os.makedirs(os.path.dirname(result_file_path), exist_ok=True)

            with open(result_file_path, 'a') as result_file:
                result_file.write(value)
                result_file.write('\n')
        else:
            print(value)

    with mysql.connector.connect(
        host=config['db']['host'],
        port=config['db']['port'],
        user=config['db']['user'],
        password=config['db']['password'],
        database=config['db']['db']
    ) as connection:
        log_query_pattern = (
            r'^(?P<level>DEBUG)' +
            r' - ' +
            r'(?P<date>\d{4}-\d{2}-\d{2}) (?P<time>\d{2}:\d{2}:\d{2})' +
            r' --> ' +
            r'(?P<json>{\"_type\":\"QUERY\".*)$'
        )
        total_scanned = 0
        total_flagged = 0

        with open(config['log']['file_path'], 'r') as logs:
            for log in logs:
                match = re.match(log_query_pattern, log)

                if match is None:
                    continue

                query_json = json.loads(match.group('json'))

                messages = scan_query(connection, query_json)
                total_scanned += 1

                if len(messages) == 0:
                    continue

                total_flagged += 1

                if 'url' in query_json:
                    url = query_json['url']
                else:
                    url = '-'

                output(f'''
Query:\n{sqlfluff.fix(query_json['query'], "mysql")}
Taken time: {query_json["time"]}ms / Cached: {query_json["using_cache"]}
URL: {url}
Called by\
''')
                for caller in query_json['caller']:
                    output('- ' + caller.replace('\\', '/'))

                output('\nMessages')
                for message in messages:
                    output(message)

                output('\n-----')

    print(f'''
Total scanned {total_scanned} queries
Total flagged {total_flagged} queries
''')


if __name__ == '__main__':
    main()
