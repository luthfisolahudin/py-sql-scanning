# SQL Scanning Tool

## Getting Started

- Rename `config.toml.example` to `config.toml` and modified to your environment
- Install required Python libraries listed in `requirements.txt`
- Run `main.py`
